<nav class="navbar navbar-expand-lg bg-light">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('test1') }}">Test 1</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('test2') }}">Test 2</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        CRUD
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="{{ route('karyawan.index') }}">Karyawan</a></li>
                        <li><a class="dropdown-item" href="{{ route('jabatan.index') }}">Jabatan</a></li>
                        <li><a class="dropdown-item" href="{{ route('levels.index') }}">Level</a></li>
                        <li><a class="dropdown-item" href="{{ route('departments.index') }}">Department</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
