<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.meta')

    <title>@yield('title') | Test ECI</title>

    @stack('before-style')

    @include('includes.style')

    @stack('after-style')
</head>

<body>
    @include('includes.header')

    <div class="container">
        @yield('content')
    </div>

    @stack('before-script')

    @include('includes.script')

    @stack('after-script')
</body>

</html>
