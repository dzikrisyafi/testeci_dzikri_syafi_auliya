@extends('layouts.app')

@section('title', 'Tambah Karyawan')

@section('content')
    <div class="row d-flex justify-content-center mt-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card p-3">
                    <div class="row">
                        <h2 class="mb-3">Tambah Karyawan</h2>
                        <div id="liveAlertPlaceholder"></div>
                        <form id="karyawan" action="{{ route('api.karyawan.store') }}" method="post">
                            <div class="mb-3">
                                <input type="text" class="form-control" id="nik" name="nik"
                                    placeholder="Masukan NIK Anda">
                                <small id="invalid-nik" class="invalid text-danger">
                                </small>
                            </div>
                            <div class="mb-3">
                                <input type="text" class="form-control" id="nama" name="nama"
                                    placeholder="Masukan Nama Anda">
                                <small id="invalid-nama" class="invalid text-danger">
                                </small>
                            </div>
                            <div class="mb-3">
                                <input type="date" class="form-control" id="ttl" name="ttl">
                                <small id="invalid-ttl" class="invalid text-danger">
                                </small>
                            </div>
                            <div class="mb-3">
                                <textarea name="alamat" id="alamat" rows="5" class="form-control" placeholder="Masukan Alamat Anda"></textarea>
                                <small id="invalid-date" class="invalid text-danger">
                                </small>
                            </div>
                            <div class="mb-3">
                                <select class="form-select" id="id_jabatan" name="id_jabatan">
                                </select>
                                <small id="invalid-id_jabatan" class="invalid text-danger">
                                </small>
                            </div>
                            <div class="mb-3">
                                <select class="form-select" id="id_dept" name="id_dept">
                                </select>
                                <small id="invalid-id_dept" class="invalid text-danger">
                                </small>
                            </div>
                            <div class="d-flex justify-content-end">
                                <a href="{{ route('karyawan.index') }}" class="btn btn-outline-secondary me-2">Kembali</a>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script type="text/javascript">
        $(document).ready(function() {
            'use strict';

            $.ajax({
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                },
                url: "{!! route('api.jabatan.index') !!}",
                success: function(response) {
                    const {
                        data
                    } = response;

                    data.forEach(element => {
                        $('#id_jabatan').append(new Option(element.nama_jabatan, element
                            .id_jabatan));
                    });
                }
            });

            $.ajax({
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                },
                url: "{!! route('api.departments.index') !!}",
                success: function(response) {
                    const {
                        data
                    } = response;

                    data.forEach(element => {
                        $('#id_dept').append(new Option(element.nama_dept, element
                            .id_dept));
                    });
                }
            });

            $('#karyawan').on('submit', function(e) {
                e.preventDefault();

                const data = new FormData(e.target);
                const value = Object.fromEntries(data.entries());
                const alertPlaceholder = $('#liveAlertPlaceholder');

                $.ajax({
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json'
                    },
                    url: $('#karyawan').attr('action'),
                    data: value,
                    success: function(data) {
                        window.location.href = "{{ route('karyawan.index') }}";
                        alertPlaceholder.html('');
                        $('.invalid').html('');
                    },
                    error: function(error) {
                        const response = error.responseJSON;

                        const alert = [
                            `<div class="alert alert-danger alert-dismissible" role="alert">`,
                            `   <div>${response.message}</div>`,
                            '   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>',
                            '</div>'
                        ].join('');

                        alertPlaceholder.html(alert);

                        if ('error' in response) {
                            const {
                                error
                            } = response;

                            for (const key in error) {
                                $(`#invalid-${key}`).html(error[key][0]);
                            }
                        }
                    }
                });
            });
        });
    </script>
@endpush
