@extends('layouts.app')

@section('title', 'Karyawan')

@section('content')
    <div class="row d-flex justify-content-center mt-4">
        <div class="col-md-10">
            <a href="{{ route('karyawan.create') }}" class="btn btn-primary mb-2">Tambah Karyawan</a>
            <div class="table-responsive">
                <table id="karyawan" class="table table-hover">
                    <thead>
                        <tr>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>Tanggal Lahir</th>
                            <th>Alamat</th>
                            <th>Jabatan</th>
                            <th>Department</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script type="text/javascript">
        $(document).ready(function() {
            'use strict';

            let _datatable;
            let dt_init = $('#karyawan');

            _datatable = dt_init.DataTable({
                ajax: {
                    url: '{!! route('api.karyawan.index') !!}',
                    dataSrc: 'data',
                },
                columns: [{
                        'data': 'nik',
                    },
                    {
                        'data': 'nama',
                    },
                    {
                        'data': 'ttl',
                    },
                    {
                        'data': 'alamat',
                    },
                    {
                        'data': 'nama_jabatan',
                    },
                    {
                        'data': 'nama_dept',
                    },
                    {
                        'data': null,
                        'render': function(data) {
                            const id = data.id_karyawan;

                            let urlEdit = "{{ route('karyawan.edit', ':id_karyawan') }}"
                            urlEdit = urlEdit.replace(':id_karyawan', id);
                            let urlDelete = "{{ route('api.karyawan.destroy', ':id_karyawan') }}"
                            urlDelete = urlDelete.replace(':id_karyawan', id);

                            return '<div class="d-flex justify-content-start"><a href="' +
                                urlEdit +
                                '" class="btn btn-warning text-white me-2">Ubah</a><a href="' +
                                urlDelete +
                                '" onclick="return confirm(\'Apakah anda yakin? \')" class="btn btn-danger text-white btn-delete">Hapus</a></div>';
                        }
                    },
                ],
                columnDefs: [{
                    orderable: false,
                    searchable: false,
                    targets: 6
                }],
            });

            dt_init.delegate('a.btn-delete', 'click', function(e) {
                e.preventDefault();

                const url = $(this).attr('href');

                $.ajax({
                    method: 'DELETE',
                    headers: {
                        'Accept': 'application/json'
                    },
                    url: url,
                    success: function(response) {
                        alert(response.message);
                        window.location.href = "{{ route('karyawan.index') }}";
                    }
                });
            });
        });
    </script>
@endpush
