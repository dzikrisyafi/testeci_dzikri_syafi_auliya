@extends('layouts.app')

@section('title', 'Jabatan')

@section('content')
    <div class="row d-flex justify-content-center mt-4">
        <div class="col-md-10">
            <a href="{{ route('jabatan.create') }}" class="btn btn-primary mb-2">Tambah Jabatan</a>
            <div class="table-responsive">
                <table id="jabatan" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Nama Jabatan</th>
                            <th>Level</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script type="text/javascript">
        $(document).ready(function() {
            'use strict';

            let _datatable;
            let dt_init = $('#jabatan');

            _datatable = dt_init.DataTable({
                ajax: {
                    url: '{!! route('api.jabatan.index') !!}',
                    dataSrc: 'data',
                },
                columns: [{
                        'data': 'nama_jabatan',
                    },
                    {
                        'data': 'nama_level',
                    },
                    {
                        'data': null,
                        'render': function(data) {
                            const id = data.id_jabatan;

                            let urlEdit = "{{ route('jabatan.edit', ':id_jabatan') }}"
                            urlEdit = urlEdit.replace(':id_jabatan', id);
                            let urlDelete = "{{ route('api.jabatan.destroy', ':id_jabatan') }}"
                            urlDelete = urlDelete.replace(':id_jabatan', id);

                            return '<div class="d-flex justify-content-start"><a href="' +
                                urlEdit +
                                '" class="btn btn-warning text-white me-2">Ubah</a><a href="' +
                                urlDelete +
                                '" onclick="return confirm(\'Apakah anda yakin? \')" class="btn btn-danger text-white btn-delete">Hapus</a></div>';
                        }
                    },
                ],
                columnDefs: [{
                    orderable: false,
                    searchable: false,
                    targets: 2
                }],
            });

            dt_init.delegate('a.btn-delete', 'click', function(e) {
                e.preventDefault();

                const url = $(this).attr('href');

                $.ajax({
                    method: 'DELETE',
                    headers: {
                        'Accept': 'application/json'
                    },
                    url: url,
                    success: function(response) {
                        alert(response.message);
                        window.location.href = "{{ route('jabatan.index') }}";
                    }
                });
            });
        });
    </script>
@endpush
