@extends('layouts.app')

@section('title', 'Ubah Jabatan')

@section('content')
    <div class="row d-flex justify-content-center mt-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card p-3">
                    <div class="row">
                        <h2 class="mb-3">Ubah Jabatan</h2>
                        <div id="liveAlertPlaceholder"></div>
                        <form id="jabatan" action="{{ route('api.jabatan.update', $id) }}" method="post">
                            <div class="mb-3">
                                <input type="text" class="form-control" id="nama_jabatan" name="nama_jabatan"
                                    placeholder="Masukan Nama Jabatan">
                                <small id="invalid-nama_jabatan" class="invalid text-danger">
                                </small>
                            </div>
                            <div class="mb-3">
                                <select class="form-select" id="id_level" name="id_level">
                                </select>
                                <small id="invalid-id_level" class="invalid text-danger">
                                </small>
                            </div>
                            <div class="d-flex justify-content-end">
                                <a href="{{ route('jabatan.index') }}" class="btn btn-outline-secondary me-2">Kembali</a>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script type="text/javascript">
        $(document).ready(function() {
            'use strict';

            $.ajax({
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                },
                url: "{!! route('api.levels.index') !!}",
                success: function(response) {
                    const {
                        data
                    } = response;

                    data.forEach(element => {
                        $('#id_level').append(new Option(element.nama_level, element
                            .id_level));
                    });
                }
            });

            $.ajax({
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                },
                url: "{!! route('api.jabatan.show', $id) !!}",
                success: function(response) {
                    const {
                        data
                    } = response;

                    $('#nama_jabatan').val(data.nama_jabatan);
                    setTimeout(function() {
                        $('#id_level').val(data.id_level).change();
                    }, 200);
                }
            });

            $('#jabatan').on('submit', function(e) {
                e.preventDefault();

                const data = new FormData(e.target);
                const value = Object.fromEntries(data.entries());
                const alertPlaceholder = $('#liveAlertPlaceholder');

                $.ajax({
                    method: 'PUT',
                    headers: {
                        'Accept': 'application/json'
                    },
                    url: $('#jabatan').attr('action'),
                    data: value,
                    success: function(data) {
                        window.location.href = "{{ route('jabatan.index') }}";
                        alertPlaceholder.html('');
                        $('.invalid').html('');
                    },
                    error: function(error) {
                        const response = error.responseJSON;

                        const alert = [
                            `<div class="alert alert-danger alert-dismissible" role="alert">`,
                            `   <div>${response.message}</div>`,
                            '   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>',
                            '</div>'
                        ].join('');

                        alertPlaceholder.html(alert);

                        if ('error' in response) {
                            const {
                                error
                            } = response;

                            for (const key in error) {
                                $(`#invalid-${key}`).html(error[key][0]);
                            }
                        }
                    }
                });
            });
        });
    </script>
@endpush
