@extends('layouts.app')

@section('title', 'Test 1')

@section('content')
    <div class="row d-flex justify-content-center mt-4">
        <div class="col-md-8">
            <div class="card p-3">
                <div class="row">
                    <div class="col-md-6">
                        <h2>No. 1</h2>
                        <div id="liveAlertPlaceholder"></div>
                        <form id="test1" action="{{ route('api.test1') }}" method="post">
                            <div class="mb-3">
                                <select class="form-select" id="tipe" name="tipe">
                                    <option value="1" selected>Tipe 1</option>
                                    <option value="2">Tipe 2</option>
                                    <option value="3">Tipe 3</option>
                                </select>
                                <small id="invalid-tipe" class="invalid text-danger">
                                </small>
                            </div>
                            <div class="mb-3">
                                <input type="text" class="form-control" id="angka" name="angka"
                                    placeholder="Masukan dalam bentuk angka lalu tekan submit">
                                <small id="invalid-angka" class="invalid text-danger">
                                </small>
                            </div>
                            <div class="d-flex justify-content-end">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <h4>Output</h4>
                        <div id="output">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script type="text/javascript">
        $(document).ready(function() {
            'use strict';

            $('#test1').on('submit', function(e) {
                e.preventDefault();

                const data = new FormData(e.target);
                const value = Object.fromEntries(data.entries());
                const alertPlaceholder = $('#liveAlertPlaceholder');

                $.ajax({
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json'
                    },
                    url: $('#test1').attr('action'),
                    data: value,
                    success: function(data) {
                        $('#output').html(data);
                        alertPlaceholder.html('');
                        $('.invalid').html('');
                    },
                    error: function(error) {
                        const response = error.responseJSON;

                        const alert = [
                            `<div class="alert alert-danger alert-dismissible" role="alert">`,
                            `   <div>${response.message}</div>`,
                            '   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>',
                            '</div>'
                        ].join('');

                        alertPlaceholder.html(alert);

                        if ('error' in response) {
                            const {
                                error
                            } = response;

                            for (const key in error) {
                                $(`#invalid-${key}`).html(error[key][0]);
                            }
                        }
                    }
                });
            });
        });
    </script>
@endpush
