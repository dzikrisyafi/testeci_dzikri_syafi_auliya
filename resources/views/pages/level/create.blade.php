@extends('layouts.app')

@section('title', 'Tambah Level')

@section('content')
    <div class="row d-flex justify-content-center mt-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card p-3">
                    <div class="row">
                        <h2 class="mb-3">Tambah Level</h2>
                        <div id="liveAlertPlaceholder"></div>
                        <form id="level" action="{{ route('api.levels.store') }}" method="post">
                            <div class="mb-3">
                                <input type="text" class="form-control" id="nama_level" name="nama_level"
                                    placeholder="Masukan Nama Level">
                                <small id="invalid-nama_level" class="invalid text-danger">
                                </small>
                            </div>
                            <div class="d-flex justify-content-end">
                                <a href="{{ route('levels.index') }}" class="btn btn-outline-secondary me-2">Kembali</a>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script type="text/javascript">
        $(document).ready(function() {
            'use strict';

            $('#level').on('submit', function(e) {
                e.preventDefault();

                const data = new FormData(e.target);
                const value = Object.fromEntries(data.entries());
                const alertPlaceholder = $('#liveAlertPlaceholder');

                $.ajax({
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json'
                    },
                    url: $('#level').attr('action'),
                    data: value,
                    success: function(data) {
                        window.location.href = "{{ route('levels.index') }}";
                        alertPlaceholder.html('');
                        $('.invalid').html('');
                    },
                    error: function(error) {
                        const response = error.responseJSON;

                        const alert = [
                            `<div class="alert alert-danger alert-dismissible" role="alert">`,
                            `   <div>${response.message}</div>`,
                            '   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>',
                            '</div>'
                        ].join('');

                        alertPlaceholder.html(alert);

                        if ('error' in response) {
                            const {
                                error
                            } = response;

                            for (const key in error) {
                                $(`#invalid-${key}`).html(error[key][0]);
                            }
                        }
                    }
                });
            });
        });
    </script>
@endpush
