@extends('layouts.app')

@section('title', 'Level')

@section('content')
    <div class="row d-flex justify-content-center mt-4">
        <div class="col-md-10">
            <a href="{{ route('levels.create') }}" class="btn btn-primary mb-2">Tambah Level</a>
            <div class="table-responsive">
                <table id="level" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Nama Level</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script type="text/javascript">
        $(document).ready(function() {
            'use strict';

            let _datatable;
            let dt_init = $('#level');

            _datatable = dt_init.DataTable({
                ajax: {
                    url: '{!! route('api.levels.index') !!}',
                    dataSrc: 'data',
                },
                columns: [{
                        'data': 'nama_level',
                    },
                    {
                        'data': null,
                        'render': function(data) {
                            const id = data.id_level;

                            let urlEdit = "{{ route('levels.edit', ':id_level') }}"
                            urlEdit = urlEdit.replace(':id_level', id);
                            let urlDelete = "{{ route('api.levels.destroy', ':id_level') }}"
                            urlDelete = urlDelete.replace(':id_level', id);

                            return '<div class="d-flex justify-content-start"><a href="' +
                                urlEdit +
                                '" class="btn btn-warning text-white me-2">Ubah</a><a href="' +
                                urlDelete +
                                '" onclick="return confirm(\'Apakah anda yakin? \')" class="btn btn-danger text-white btn-delete">Hapus</a></div>';
                        }
                    },
                ],
                columnDefs: [{
                    orderable: false,
                    searchable: false,
                    targets: 1
                }],
            });

            dt_init.delegate('a.btn-delete', 'click', function(e) {
                e.preventDefault();

                const url = $(this).attr('href');

                $.ajax({
                    method: 'DELETE',
                    headers: {
                        'Accept': 'application/json'
                    },
                    url: url,
                    success: function(response) {
                        alert(response.message);
                        window.location.href = "{{ route('levels.index') }}";
                    }
                });
            });
        });
    </script>
@endpush
