@extends('layouts.app')

@section('title', 'Department')

@section('content')
    <div class="row d-flex justify-content-center mt-4">
        <div class="col-md-10">
            <a href="{{ route('departments.create') }}" class="btn btn-primary mb-2">Tambah Department</a>
            <div class="table-responsive">
                <table id="department" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Nama Department</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script type="text/javascript">
        $(document).ready(function() {
            'use strict';

            let _datatable;
            let dt_init = $('#department');

            _datatable = dt_init.DataTable({
                ajax: {
                    url: '{!! route('api.departments.index') !!}',
                    dataSrc: 'data',
                },
                columns: [{
                        'data': 'nama_dept',
                    },
                    {
                        'data': null,
                        'render': function(data) {
                            const id = data.id_dept;

                            let urlEdit = "{{ route('departments.edit', ':id_dept') }}"
                            urlEdit = urlEdit.replace(':id_dept', id);
                            let urlDelete = "{{ route('api.departments.destroy', ':id_dept') }}"
                            urlDelete = urlDelete.replace(':id_dept', id);

                            return '<div class="d-flex justify-content-start"><a href="' +
                                urlEdit +
                                '" class="btn btn-warning text-white me-2">Ubah</a><a href="' +
                                urlDelete +
                                '" onclick="return confirm(\'Apakah anda yakin? \')" class="btn btn-danger text-white btn-delete">Hapus</a></div>';
                        }
                    },
                ],
                columnDefs: [{
                    orderable: false,
                    searchable: false,
                    targets: 1
                }],
            });

            dt_init.delegate('a.btn-delete', 'click', function(e) {
                e.preventDefault();

                const url = $(this).attr('href');

                $.ajax({
                    method: 'DELETE',
                    headers: {
                        'Accept': 'application/json'
                    },
                    url: url,
                    success: function(response) {
                        alert(response.message);
                        window.location.href = "{{ route('departments.index') }}";
                    }
                });
            });
        });
    </script>
@endpush
