@extends('layouts.app')

@section('title', 'Test 2')

@section('content')
    <div class="row d-flex justify-content-center mt-4">
        <div class="col-md-8">
            <div class="card p-3">
                <div class="row">
                    <div class="col-md-6">
                        <h2>No. 2</h2>
                        <div id="liveAlertPlaceholder"></div>
                        <form id="test2" action="{{ route('api.test2') }}" method="post">
                            <div class="mb-3">
                                <input type="text" class="form-control" id="angka" name="angka"
                                    placeholder="Masukan angka lalu tekan submit contoh: 9834201">
                                <small id="invalid-angka" class="invalid text-danger">
                                </small>
                            </div>
                            <div class="d-flex justify-content-end">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <h4>Output</h4>
                        <div id="output">
                            <span id="nominal"></span>
                            <h3 id="terbilang"></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-script')
    <script type="text/javascript">
        $(document).ready(function() {
            'use strict';

            $('#test2').on('submit', function(e) {
                e.preventDefault();

                const data = new FormData(e.target);
                const value = Object.fromEntries(data.entries());
                const alertPlaceholder = $('#liveAlertPlaceholder');

                $.ajax({
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json'
                    },
                    url: $('#test2').attr('action'),
                    data: value,
                    success: function(response) {
                        const {
                            data
                        } = response;

                        const output = [
                            `<h5 id="nominal" class="text-secondary">${data.nominal}</h5>`,
                            `<h5 class="text-secondary">Terbilang :</h5>`,
                            `<h3 id="terbilang">${data.terbilang}</h3>`
                        ].join('');

                        $('#output').html(output);

                        alertPlaceholder.html('');
                        $('.invalid').html('');
                    },
                    error: function(error) {
                        const response = error.responseJSON;

                        const alert = [
                            `<div class="alert alert-danger alert-dismissible" role="alert">`,
                            `   <div>${response.message}</div>`,
                            '   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>',
                            '</div>'
                        ].join('');

                        alertPlaceholder.html(alert);

                        if ('error' in response) {
                            const {
                                error
                            } = response;

                            for (const key in error) {
                                $(`#invalid-${key}`).html(error[key][0]);
                            }
                        }
                    }
                });
            });
        });
    </script>
@endpush
