<?php

use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\JabatanController;
use App\Http\Controllers\KaryawanController;
use App\Http\Controllers\LevelController;
use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect()->route('test1');
});

Route::get('/tests/test1', [TestController::class, 'test1'])->name('test1');
Route::get('/tests/test2', [TestController::class, 'test2'])->name('test2');

Route::resource('karyawan', KaryawanController::class)->except('show');
Route::resource('jabatan', JabatanController::class)->except('show');
Route::resource('levels', LevelController::class)->except('show');
Route::resource('departments', DepartmentController::class)->except('show');
