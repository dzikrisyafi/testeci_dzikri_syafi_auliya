<?php

use App\Http\Controllers\Api\DepartmentController;
use App\Http\Controllers\Api\JabatanController;
use App\Http\Controllers\Api\KaryawanController;
use App\Http\Controllers\Api\LevelController;
use App\Http\Controllers\Api\TestController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::name('api.')->group(function () {
    Route::post('tests/test1', [TestController::class, 'test1'])->name('test1');
    Route::post('tests/test2', [TestController::class, 'test2'])->name('test2');

    Route::apiResources([
        'karyawan' => KaryawanController::class,
        'jabatan' => JabatanController::class,
        'levels' => LevelController::class,
        'departments' => DepartmentController::class,
    ]);
});
