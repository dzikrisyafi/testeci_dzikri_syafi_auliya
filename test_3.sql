-- no 1
CREATE TABLE karyawan(
	id_karyawan INT AUTO_INCREMENT PRIMARY KEY,
	nik VARCHAR(10) NOT NULL UNIQUE,
	nama VARCHAR(100) NOT NULL,
	ttl DATE NOT NULL,
	alamat TEXT,
	id_jabatan INT,
	id_dept INT
);

CREATE TABLE jabatan(
	id_jabatan INT AUTO_INCREMENT PRIMARY KEY,
	nama_jabatan VARCHAR(100) NOT NULL,
	id_level INT
);

CREATE TABLE level(
	id_level INT AUTO_INCREMENT PRIMARY KEY,
	nama_level VARCHAR(100) NOT NULL
);

CREATE TABLE department(
	id_dept INT AUTO_INCREMENT PRIMARY KEY,
	nama_dept VARCHAR(100) NOT NULL
);

-- add foreign keys to table
ALTER TABLE karyawan ADD FOREIGN KEY (id_jabatan) REFERENCES jabatan(id_jabatan) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE karyawan ADD FOREIGN KEY (id_dept) REFERENCES department(id_dept) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE jabatan ADD FOREIGN KEY (id_level) REFERENCES level(id_level) ON DELETE SET NULL ON UPDATE CASCADE;

-- no 2
INSERT INTO department (nama_dept)
VALUES
('Penjualan'),
('Human Resource'),
('Produksi');

INSERT INTO level (nama_level)
VALUES
('Entry Level'),
('Intermediate'),
('Lower Management');

INSERT INTO jabatan (nama_jabatan, id_level)
VALUES
('Sales Admin', 1),
('HR Analyst', 2),
('Project Manager', 3);

INSERT INTO karyawan (nik, nama, ttl, alamat, id_jabatan, id_dept)
VALUES
('32013713', 'Dzikri', '1999-07-13', 'Komp. Inkopad Blok E4 No.10', 1, 1),
('32013714', 'Syafi', '1999-08-14', 'Komp. Inkopad Blok E5 No.11', 2, 2),
('32013715', 'Auliya', '1999-09-15', 'Komp. Inkopad Blok E6 No.12', 3, 3);

-- no 3
SELECT
nama as nama_karyawan,
nama_jabatan,
nama_level,
nama_dept as nama_department
FROM karyawan k
INNER JOIN jabatan j ON (j.id_jabatan=k.id_jabatan)
INNER JOIN level l ON (l.id_level=j.id_level)
INNER JOIN department d ON (d.id_dept=k.id_dept);

-- no 4
UPDATE karyawan SET nama = 'Ghifari', ttl = '1998-06-02', alamat = 'Komp. Gaperi Blok E5 No.11' WHERE id_karyawan = 2;

-- no 5
DELETE FROM karyawan WHERE id_karyawan = 3;