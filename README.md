## Laravel & PHP Version

| Laravel        | PHP             |
|----------------|-----------------|
| ^10.10         | ^8.1            |

## Installation

First, you have to install all the default packages in this project:

```bash
composer install
```

Now after you install all the default packages you need to update your .env configuration file to use the appropriate database, for example:

```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=testeci
DB_USERNAME=root
DB_PASSWORD=
```

After that you can run this command to generate all tables and data seeder:

```bash
php artisan migrate:fresh --seed
```

Or, if you just want to generate all tables without data seeder you can run:

```bash
php artisan migrate:fresh
```

Finally, before you run this project you have to run this command to generate app key on env file:

```bash
php artisan key:generate
```

## Usage

Once the project has been installed, start Laravel's local development server using Laravel Artisan's serve command:

```bash
php artisan serve
```

Once you have started the Artisan development server, your application will be accessible in your web browser at http://localhost:8000.
