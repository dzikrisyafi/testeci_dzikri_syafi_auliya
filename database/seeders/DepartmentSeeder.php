<?php

namespace Database\Seeders;

use App\Models\Department;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $departments = [
            [
                'nama_dept' => 'Penjualan'
            ],
            [
                'nama_dept' => 'Human Resource'
            ],
            [
                'nama_dept' => 'Produksi'
            ],
        ];

        Department::insert($departments);
    }
}
