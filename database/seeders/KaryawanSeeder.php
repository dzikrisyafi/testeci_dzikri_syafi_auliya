<?php

namespace Database\Seeders;

use App\Models\Karyawan;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class KaryawanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $karyawan = [
            [
                'nik' => '32013713',
                'nama' => 'Dzikri',
                'ttl' => '1999-07-13',
                'alamat' => 'Komp. Inkopad Blok E4 No.10',
                'id_jabatan' => 1,
                'id_dept' => 1,
            ],
            [
                'nik' => '32013714',
                'nama' => 'Syafi',
                'ttl' => '1999-08-14',
                'alamat' => 'Komp. Inkopad Blok E5 No.11',
                'id_jabatan' => 2,
                'id_dept' => 2,
            ],
            [
                'nik' => '32013715',
                'nama' => 'Auliya',
                'ttl' => '1999-09-15',
                'alamat' => 'Komp. Inkopad Blok E6 No.12',
                'id_jabatan' => 3,
                'id_dept' => 3,
            ],
        ];

        Karyawan::insert($karyawan);
    }
}
