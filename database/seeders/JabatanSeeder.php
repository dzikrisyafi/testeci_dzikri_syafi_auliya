<?php

namespace Database\Seeders;

use App\Models\Jabatan;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class JabatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $jabatan = [
            [
                'nama_jabatan' => 'Sales Admin',
                'id_level' => 1,
            ],
            [
                'nama_jabatan' => 'HR Analyst',
                'id_level' => 2,
            ],
            [
                'nama_jabatan' => 'Project Manager',
                'id_level' => 3,
            ],
        ];

        Jabatan::insert($jabatan);
    }
}
