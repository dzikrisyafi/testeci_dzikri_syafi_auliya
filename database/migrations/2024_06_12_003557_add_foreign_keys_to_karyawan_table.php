<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('karyawan', function (Blueprint $table) {
            $table->foreign('id_jabatan', 'fk_karyawan_to_jabatan')
                ->references('id_jabatan')
                ->on('jabatan')
                ->nullOnDelete()
                ->cascadeOnUpdate();

            $table->foreign('id_dept', 'fk_karyawan_to_department')
                ->references('id_dept')
                ->on('department')
                ->nullOnDelete()
                ->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('karyawan', function (Blueprint $table) {
            $table->dropForeign('fk_karyawan_to_jabatan');
            $table->dropForeign('fk_karyawan_to_department');
        });
    }
};
