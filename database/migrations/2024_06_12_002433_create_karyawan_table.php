<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('karyawan', function (Blueprint $table) {
            $table->id('id_karyawan');
            $table->string('nik', 10)->unique();
            $table->string('nama', 100);
            $table->date('ttl');
            $table->text('alamat');
            $table->foreignId('id_jabatan')->nullable()->index('fk_karyawan_to_jabatan');
            $table->foreignId('id_dept')->nullable()->index('fk_karyawan_to_department');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('karyawan');
    }
};
