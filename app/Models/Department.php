<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;

    protected $table = 'department';

    protected $primaryKey = 'id_dept';

    protected $fillable = [
        'nama_dept',
    ];

    public $timestamps = false;

    public function karyawans()
    {
        return $this->hasMany(Karyawan::class, 'id_dept', 'id_dept');
    }
}
