<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    use HasFactory;

    protected $table = 'level';

    protected $primaryKey = 'id_level';

    protected $fillable = [
        'nama_level',
    ];

    public $timestamps = false;

    public function jabatans()
    {
        return $this->hasMany(Jabatan::class, 'id_level', 'id_level');
    }
}
