<?php

namespace App\Providers;

use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Response::macro('success', function ($status_code, $message = null, $data = null) {
            $response = [
                'code' => $status_code,
                'status' => HttpResponse::$statusTexts[$status_code]
            ];
            if ($message) $response['message'] = $message;
            if ($data) $response['data'] = $data;

            return response()->json($response, $status_code);
        });

        Response::macro('error', function ($status_code, $message = null, $error = null) {
            $response = [
                'code' => $status_code,
                'status' => HttpResponse::$statusTexts[$status_code]
            ];
            if ($message) $response['message'] = $message;
            if ($error) $response['error'] = $error;

            return response()->json($response, $status_code);
        });
    }
}
