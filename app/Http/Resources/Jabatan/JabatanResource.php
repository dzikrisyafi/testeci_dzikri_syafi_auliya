<?php

namespace App\Http\Resources\Jabatan;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class JabatanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id_jabatan' => $this->id_jabatan,
            'nama_jabatan' => $this->nama_jabatan,
            'nama_level' => $this->level->nama_level,
        ];
    }
}
