<?php

namespace App\Http\Resources\Karyawan;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class KaryawanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id_karyawan' => $this->id_karyawan,
            'nik' => $this->nik,
            'nama' => $this->nama,
            'ttl' => $this->ttl,
            'alamat' => $this->alamat,
            'nama_jabatan' => $this->jabatan->nama_jabatan,
            'nama_dept' => $this->department->nama_dept,
        ];
    }
}
