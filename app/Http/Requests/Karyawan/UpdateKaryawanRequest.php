<?php

namespace App\Http\Requests\Karyawan;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;

class UpdateKaryawanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'nik' => ['required', 'string', 'max:10', Rule::unique('karyawan')->ignore($this->route('karyawan'), 'id_karyawan')],
            'nama' => ['required', 'string', 'max:100'],
            'ttl' => ['required', 'date'],
            'alamat' => ['nullable', 'string', 'max:255'],
            'id_jabatan' => ['required', 'exists:jabatan,id_jabatan'],
            'id_dept' => ['required', 'exists:department,id_dept'],
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->error(Response::HTTP_UNPROCESSABLE_ENTITY, 'The given data was invalid.', $validator->errors()));
    }
}
