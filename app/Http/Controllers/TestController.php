<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function test1()
    {
        return view('pages.test1');
    }

    public function test2()
    {
        return view('pages.test2');
    }
}
