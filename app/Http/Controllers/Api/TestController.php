<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Test\Test1Request;
use App\Http\Requests\Test\Test2Request;
use Illuminate\Http\Response;
use Riskihajar\Terbilang\Facades\Terbilang;

class TestController extends Controller
{
    public function test1(Test1Request $request)
    {
        $validated = $request->validated();

        switch ($validated['tipe']) {
            case 1:
                segitiga1($validated['angka']);
                break;
            case 2:
                segitiga2($validated['angka']);
                break;
            case 3:
                segitiga3($validated['angka']);
                break;
            default:
                return response()->error(Response::HTTP_BAD_REQUEST, 'The selected tipe is invalid.');
        }
    }

    public function test2(Test2Request $request)
    {
        $validated = $request->validated();

        return response()->success(Response::HTTP_OK, null, [
            'nominal' => rupiah_currency($validated['angka']),
            'terbilang' => Terbilang::make($validated['angka'], ' rupiah'),
        ]);
    }
}
