<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Jabatan\StoreJabatanRequest;
use App\Http\Requests\Jabatan\UpdateJabatanRequest;
use App\Http\Resources\Jabatan\JabatanResource;
use App\Models\Jabatan;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class JabatanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $jabatan = Jabatan::all();

        return response()->success(Response::HTTP_OK, null, JabatanResource::collection($jabatan));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreJabatanRequest $request)
    {
        $validated = $request->validated();

        $jabatan = Jabatan::create($validated);

        return response()->success(Response::HTTP_CREATED, 'Jabatan created successfully.', [
            'id_jabatan' => $jabatan->id_jabatan,
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $jabatan = Jabatan::findOrFail($id);

        return response()->success(Response::HTTP_OK, null, $jabatan);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateJabatanRequest $request, string $id)
    {
        $validated = $request->validated();

        $jabatan = Jabatan::findOrFail($id);
        $jabatan->update($validated);

        return response()->success(Response::HTTP_OK, 'Jabatan updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $jabatan = Jabatan::findOrFail($id);

        $jabatan->delete();

        return response()->success(Response::HTTP_OK, 'Jabatan deleted successfully.');
    }
}
