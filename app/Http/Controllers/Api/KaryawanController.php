<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Karyawan\StoreKaryawanRequest;
use App\Http\Requests\Karyawan\UpdateKaryawanRequest;
use App\Http\Resources\Karyawan\KaryawanResource;
use App\Models\Karyawan;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class KaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $karyawan = Karyawan::all();

        return response()->success(Response::HTTP_OK, null, KaryawanResource::collection($karyawan));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreKaryawanRequest $request)
    {
        $validated = $request->validated();

        $karyawan = Karyawan::create($validated);

        return response()->success(Response::HTTP_CREATED, 'Karyawan created successfully.', [
            'id_karyawan' => $karyawan->id_karyawan,
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $karyawan = Karyawan::findOrFail($id);

        return response()->success(Response::HTTP_OK, null, $karyawan);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateKaryawanRequest $request, string $id)
    {
        $validated = $request->validated();

        $karyawan = Karyawan::findOrFail($id);
        $karyawan->update($validated);

        return response()->success(Response::HTTP_OK, 'Karyawan updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $karyawan = Karyawan::findOrFail($id);

        $karyawan->delete();

        return response()->success(Response::HTTP_OK, 'Karyawan deleted successfully.');
    }
}
