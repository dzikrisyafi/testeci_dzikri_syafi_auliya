<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Level\StoreLevelRequest;
use App\Http\Requests\Level\UpdateLevelRequest;
use App\Models\Level;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LevelController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $levels = Level::all();

        return response()->success(Response::HTTP_OK, null, $levels);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreLevelRequest $request)
    {
        $validated = $request->validated();

        $level = Level::create($validated);

        return response()->success(Response::HTTP_CREATED, 'Level created successfully.', [
            'id_level' => $level->id_level,
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $level = Level::findOrFail($id);

        return response()->success(Response::HTTP_OK, null, $level);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateLevelRequest $request, string $id)
    {
        $validated = $request->validated();

        $level = Level::findOrFail($id);
        $level->update($validated);

        return response()->success(Response::HTTP_OK, 'Level updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $level = Level::findOrFail($id);

        $level->delete();

        return response()->success(Response::HTTP_OK, 'Level deleted successfully.');
    }
}
