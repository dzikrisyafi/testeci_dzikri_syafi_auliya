<?php

if (!function_exists('segitiga1')) {
    function segitiga1($angka)
    {
        for ($i = 0; $i < $angka; $i++) {
            for ($j = 0; $j <= $i; $j++) {
                echo "* ";
            }
            echo "<br>";
        }
    }
}

if (!function_exists('segitiga2')) {
    function segitiga2($angka)
    {
        for ($i = 0; $i < $angka; $i++) {
            for ($j = $angka - $i; $j >= 1; $j--) {
                echo "* ";
            }
            echo "<br>";
        }
    }
}

if (!function_exists('segitiga3')) {
    function segitiga3($angka)
    {
        for ($i = 1; $i <= $angka; $i++) {
            for ($j = 1; $j <= $angka; $j++) {
                if ($j <= ($angka - $i)) {
                    echo "&nbsp;" . "&nbsp;";
                } else {
                    echo "* ";
                }
            }
            echo "<br>";
        }
    }
}

if (!function_exists('rupiah_currency')) {
    function rupiah_currency($angka)
    {
        return 'Rp. ' . number_format($angka, 0);
    }
}
