<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            // if ($request->is('api/*')) {
            //     if ($e instanceof ValidationException) {
            //         return response()->error(Response::HTTP_UNPROCESSABLE_ENTITY, 'Validation error', $e->errors());
            //     }

            //     if ($e instanceof NotFoundHttpException) {
            //         return response()->error(Response::HTTP_NOT_FOUND, 'Resource not found.');
            //     }

            //     if ($e instanceof MethodNotAllowedHttpException) {
            //         return response()->error(Response::HTTP_METHOD_NOT_ALLOWED, $e->getMessage());
            //     }

            //     if ($e instanceof AccessDeniedHttpException) {
            //         return response()->error(Response::HTTP_FORBIDDEN, 'This action is unauthorized.');
            //     }

            //     if ($e instanceof HttpResponseException) {
            //         $e = $e->getResponse();
            //     }

            //     $statusCode = method_exists($e, 'getStatusCode') ? $e->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;
            //     if ($statusCode === Response::HTTP_INTERNAL_SERVER_ERROR) {
            //         return response()->error(Response::HTTP_INTERNAL_SERVER_ERROR, 'Sorry, there was a failure on our server.');
            //     }
            // }
        });

        $this->renderable(function (Exception $exception, Request $request) {
            if ($request->is('api/*')) {
                $exception = $this->prepareException($exception);

                if ($exception instanceof ValidationException) {
                    return response()->error(Response::HTTP_UNPROCESSABLE_ENTITY, 'Validation error', $exception->errors());
                }

                if ($exception instanceof NotFoundHttpException) {
                    return response()->error(Response::HTTP_NOT_FOUND, 'Resource not found.');
                }

                if ($exception instanceof MethodNotAllowedHttpException) {
                    return response()->error(Response::HTTP_METHOD_NOT_ALLOWED, $exception->getMessage());
                }

                if ($exception instanceof AccessDeniedHttpException) {
                    return response()->error(Response::HTTP_FORBIDDEN, 'This action is unauthorized.');
                }

                if ($exception instanceof HttpResponseException) {
                    $exception = $exception->getResponse();
                }

                $statusCode = method_exists($exception, 'getStatusCode') ? $exception->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;
                if ($statusCode === Response::HTTP_INTERNAL_SERVER_ERROR) {
                    return response()->error(Response::HTTP_INTERNAL_SERVER_ERROR, 'Sorry, there was a failure on our server.');
                }
            }
        });
    }
}
